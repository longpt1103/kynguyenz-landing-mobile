// window.GapoScript = (() => {
//   const utils = {
//     isMobile: (agent) =>
//       /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
//         agent || window.navigator.userAgent
//       ),
//     fixIE: () => {
//       if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
//         const msViewportStyle = document.createElement("style");
//         msViewportStyle.appendChild(
//           document.createTextNode("@-ms-viewport{width:auto!important}")
//         );
//         document.querySelector("head").appendChild(msViewportStyle);
//       }
//     },
//   };

//   const initSearchBox = () => {
//     const $searchInput = $("#js-search-input");
//     const $searchBox = $("#js-search-box");

//     $searchInput
//       .on("focus", () => {
//         $searchBox.addClass("header__search-box--focus");
//       })
//       .on("blur", () => {
//         $searchBox.removeClass("header__search-box--focus");
//       });
//   };
//   const handleDarkMode = () => {
//     const $DarkMode = $("#js-dark-mode");
//     $("#js-dark-mode").on("click", () => {
//       $("html").toggleClass("dark-mode");
//     });
//   };
//   const handleSearch = () => {
//     const $btnSearch = $("#btnSearch");
//     $("#btnSearch").on("click", () => {
//       $("#boxSearch").toggle();
//     });
//   };
//   const toggleMenu = () => {
//     const $btnMenuToggle = $(".icon-toggle");
//     $(".icon-toggle").on("click",() => {
//       $(this).toggleClass("current-toggle");
//       $(this).next().toggle();
//     });
//   };
//   const showMenu = () => {
//     const $btnShowMenu = $("#showMenu");
//     const $btnHideMenu = $("#close-menu");
//     const $btnMenuModal = $(".menu-modal");
//     $("#showMenu").on("click",() => {
//       $('#menu-mobi').slideDown(500);
//     });
//     $("#close-menu").on("click",() => {
//       $('#menu-mobi').slideUp(500);
//     });
//     $(".menu-modal").on("click",() => {
//       $('#menu-mobi').slideUp(100);
//     });
//   };
  

//   const init = () => {
//     /* Fix IE */
//     utils.fixIE();

//     initSearchBox();
//     handleDarkMode();
//     handleSearch();
//     toggleMenu();
//     showMenu();
//   };

//   return {
//     init,
//   };
// })();

// $(document).ready(() => {
//   window.GapoScript.init();
// });

$(document).ready(function(){
  // $(window).bind("scroll", function () {
  //   if ($(window).scrollTop() > 175) {
  //     $(".gametv-navbar").addClass("fixed");
  //   } else {
  //     $(".gametv-navbar").removeClass("fixed");
  //   }
  //   scrollFunction();
  // });
  $("#js-dark-mode").on("click", function(){
    $("html").toggleClass("dark-mode");
  });
  $("#btnSearch").on("click", function(){
    $("#boxSearch").toggle();
  });
  $(".icon-toggle").on("click",function(){
    $(this).toggleClass("current-toggle");
    $(this).next().toggle();
  });
  $("#showMenu").on("click",function(){
    $('#menu-mobi').slideDown(500);
  });
  $("#close-menu").on("click",function(){
    $('#menu-mobi').slideUp(500);
  });
  $(".menu-modal").on("click",function(){
    $('#menu-mobi').slideUp(100);
  });
  
  
  
});

// backtotop
// var mybutton = document.getElementById("backTop");

// function topFunction() {
//   document.body.scrollTop = 0;
//   document.documentElement.scrollTop = 0;
// }
// function scrollFunction() {
//   if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
//     mybutton.style.display = "block";
//   } else {
//     mybutton.style.display = "none";
//   }
// }

var swiper = new Swiper('.swiper-container', {
  spaceBetween: 30,
  effect: 'fade',
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

var swiper2 = new Swiper('.swiper-container-2', {
  effect: 'coverflow',
  grabCursor: true,
  centeredSlides: true,
  slidesPerView: 'auto',
  coverflowEffect: {
    rotate: 50,
    stretch: 0,
    depth: 100,
    modifier: 1,
    slideShadows: true,
  },
  pagination: {
    el: '.swiper-pagination',
  },
});


var swiperHero = new Swiper('.swiper-hero', {
  effect: 'fade',
  spaceBetween: 30,
  centeredSlides: true,
  autoplay: {
    delay: 40000,
    disableOnInteraction: false,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  pagination: {
    el: '.swiper-pagination-hero',
    clickable: true,
    renderBullet: function (index, className) {
      return '<span class="' + className + '">' + '</span>';
    },
  },
});
